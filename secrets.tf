# ---------------------------------------------------------------------------------------------------------------------
# dev secrets
# ---------------------------------------------------------------------------------------------------------------------
variable "AWS_ACCESS_KEY_ID" {
  description = "AWS KEY ID for Development"
}
variable "AWS_SECRET_ACCESS_KEY" {
  description = "AWS SECRET KEY for Development"
}

variable "REGISTRY_USERNAME" {
  description = "Docker Registry Username for Development"
}

variable "REGISTRY_PASSWORD" {
  description = "Docker Registry Password for Development"
}

variable "REGISTRY_EMAIL" {
  description = "Docker Registry Server for Development"
}