resource "kubernetes_secret" "myregistrykey" {
  metadata {
    name      = "myregistrykey"
    namespace = kubernetes_namespace_v1.ns_ngrx_data.metadata[0].name    
  }

  type = "kubernetes.io/dockerconfigjson"

  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "https://index.docker.io/v1/" = {
          "username" = var.REGISTRY_USERNAME
          "password" = var.REGISTRY_PASSWORD
          "email"    = var.REGISTRY_EMAIL
          "auth"     = base64encode("${var.REGISTRY_USERNAME}:${var.REGISTRY_PASSWORD}")
        }
      }
    })
  }
}
