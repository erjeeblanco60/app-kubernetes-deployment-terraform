# Terraform Remote State Datasource - Remote Backend AWS S3
data "terraform_remote_state" "eks" {
  backend = "s3"
  config = {
    bucket = "<YOUR_BUCKET_NAME>"
    key    = "<YOUR_BUCKET_KEY>"
    region = "<YOUR_BUCKET_REGION>"
  }
}