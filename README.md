# App Deployment Terraform Deployment

This repository contains the Terraform manifests for deploying a Node.js application container to a Kubernetes cluster. The deployment utilizes SOPS encryption with AWS Key Management Service (KMS) to encrypt the AWS secret credentials into secret files.

## Prerequisites

Before deploying the application, make sure you have the following prerequisites:

- Access to a Kubernetes cluster
- Terraform installed on your local machine
- AWS CLI configured with valid credentials
- SOPS installed on your local machine
- AWS KMS key configured for encryption

## Manifest Files

The repository includes the following Terraform manifest files:

- `remote-state-datasource.tf`: Configures the remote state backend to store the Terraform state.
- `providers.tf`: Defines the AWS, SOPS, and Kubernetes providers.
- `configmap.tf`: Creates a ConfigMap in Kubernetes, which holds configuration data for the application.
- `deployment.tf`: Defines the deployment resource for running the Node.js application container.
- `secrets.tf`: Creates a secret in Kubernetes, encrypted using SOPS, to store sensitive data such as AWS credentials.
- `nodeport-service.tf`: Configures a NodePort service to expose the application to external traffic.
- `ingress-service.tf`: Sets up an Ingress resource to enable external access to the application using an Ingress controller.
- `namespace.tf`: Creates a namespace to isolate the application resources.
- `acm-certificate.tf`: Manages an AWS Certificate Manager (ACM) certificate for securing HTTPS traffic.

## Deployment Steps

To deploy the Node.js application to your Kubernetes cluster, follow these steps:

1. Ensure you have the prerequisites mentioned above.
2. Set up the necessary AWS IAM roles and permissions for the Terraform deployment.
3. Encrypt your AWS secret credentials using SOPS and the configured AWS KMS key.
4. Place the encrypted secret file in the appropriate location, as referenced in the `secret.tf` file.
5. Run `terraform init` to initialize the Terraform configuration.
6. Run `terraform plan` to see a summary of the deployment plan.
7. If the plan looks satisfactory, execute `terraform apply` to deploy the Node.js application to the Kubernetes cluster.
8. Monitor the deployment process for any errors or issues.
9. Once the deployment is successful, you can access the application using the configured ingress URL.

## Execute the following commands:

```
# Change with appropriate value(s)
SECRET_KMS=arn:aws:kms:eu-west-1:******:alias/sops-key

# Decode secrets
sops --decrypt --kms ${SECRET_KMS} secrets.enc.yaml > secrets.dec.yaml

# Generate Terraform variables file for secrets
gomplate --datasource secrets=secrets.dec.yaml --file secrets.tfvars.tpl --out secrets.dec.tfvars

# Plan terraform using the var file generated
terraform plan -var-file=secrets.dec.tfvars

# Apply terraform
terraform apply -var-file="secrets.dec.tfvars" -auto-approve
```

Never commit the `secrets.dec.*` files! They are included here only for demonstration purposes.


Remember to follow best practices and security measures while handling sensitive data and managing the deployment environment.

## Conclusion

This Terraform deployment simplifies the process of deploying a Node.js application container to a Kubernetes cluster. By leveraging SOPS encryption with AWS KMS, sensitive data is securely stored in encrypted files. Feel free to explore the manifest files and customize them according to your specific application requirements. Happy deploying!