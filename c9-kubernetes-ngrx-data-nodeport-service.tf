# Kubernetes Service Manifest (Type: Node Port Service)
resource "kubernetes_service_v1" "ngrx_data_np_service" {
  metadata {
    name = "ngrx-data-nodeport-service"
    annotations = {
      "alb.ingress.kubernetes.io/healthcheck-path" = "/api/health"
    }    
    namespace = kubernetes_namespace_v1.ns_ngrx_data.metadata[0].name    
  }
  spec {
    selector = {
      app = kubernetes_deployment_v1.ngrx_data.spec.0.selector.0.match_labels.app
    }
    port {
      name        = "http"
      port        = 80
      target_port = 4545
    }
    type = "NodePort"
  }
}
