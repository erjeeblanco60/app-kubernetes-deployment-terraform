{{ $secrets := (datasource "secrets").secrets -}}
{{ range $e := $secrets -}}
{{ range $s := $e.secrets -}}
{{ $s.name }} = "{{ $s.value }}"
{{ end -}}
{{ end -}}