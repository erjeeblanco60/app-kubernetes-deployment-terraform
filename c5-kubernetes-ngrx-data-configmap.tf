resource "kubernetes_config_map" "ngrx_data" {
  metadata {
    name      = "my-configmap"
    namespace = kubernetes_namespace_v1.ns_ngrx_data.metadata[0].name    
    labels = {
      app = "my-app"
    }
  }

  data = {
    AWS_DEFAULT_REGION = "<REGION>", // Change to your desired region
    TABLE_NAME = "<YOUR_TABLE_NAME>",
    BUCKET_NAME = "<YOUR_BUCKET_NAME>",
    PORT: 4545
  }
}
