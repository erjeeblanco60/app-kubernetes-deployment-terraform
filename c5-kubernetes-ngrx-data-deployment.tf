# Kubernetes Deployment Manifest
resource "kubernetes_deployment_v1" "ngrx_data" {
  metadata {
    name = "app-deployment"
    labels = {
      app = "app"
    }
    namespace = kubernetes_namespace_v1.ns_ngrx_data.metadata[0].name    
  } 
 
  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "app"
      }
    }

    template {
      metadata {
        labels = {
          app = "app"
        }
      }
      spec {
        image_pull_secrets {
          name = kubernetes_secret.myregistrykey.metadata[0].name
        }
        container {
          image = "erjeeconchas01/node-s3-upload:data"
          name  = "app"
          port {
            container_port = 4545
          }
          
              env_from {
                secret_ref {
                  name = kubernetes_secret.ngrx_data.metadata[0].name
                }

            }

            env_from {
                config_map_ref {
                  name = kubernetes_config_map.ngrx_data.metadata[0].name
                }
            }
          }
        }
      }
    }
}

