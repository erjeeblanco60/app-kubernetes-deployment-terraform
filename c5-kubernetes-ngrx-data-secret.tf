resource "kubernetes_secret" "ngrx_data" {
  metadata {
    name      = "my-secret"
    namespace = kubernetes_namespace_v1.ns_ngrx_data.metadata[0].name    
    labels = {
      app = "my-app"
    }
  }

  data = {
    AWS_ACCESS_KEY_ID = var.AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY = var.AWS_SECRET_ACCESS_KEY
  }

  type = "Opaque"
}
