# Terraform Settings Block
terraform {
  required_version = ">= 1.0.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      #version = "~> 4.4"
      version = ">= 4.65"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      #version = "~> 2.7"
      version = ">= 2.20"
    }

    sops = {
      source = "carlpett/sops"
      version = "~> 0.5"
    }

  }
  # Adding Backend as S3 for Remote State Storage
  backend "s3" {
    bucket = "<YOUR_BUCKET_NAME>"
    key    = "<YOUR_BUCKET_KEY>"
    region = "<YOUR_BUCKET_REGION>" 

    # For State Locking
    dynamodb_table = "<YOUR_DYNAMODB_TABLE_NAME>"    
  }    
}
