{{ $secrets := (datasource "secrets").secrets -}}
{{ range $e := $secrets -}}
# ---------------------------------------------------------------------------------------------------------------------
# {{ $e.environment }} secrets
# ---------------------------------------------------------------------------------------------------------------------
{{ range $s := $e.secrets -}}
variable "{{ $s.name }}" {
  description = "{{ $s.description }}"
}
resource "aws_secretsmanager_secret" "{{ $s.name }}" {
  name                    = "{{ $s.name }}"
  description             = "{{ $s.description }}"
  recovery_window_in_days = 7
}
resource "aws_secretsmanager_secret_version" "{{ $s.name }}" {
  secret_id     = "${aws_secretsmanager_secret.{{ $s.name }}.id}"
  secret_string = "${var.{{ $s.name }}}"
}
{{ end -}}
{{ end -}}